#include <stdio.h>
#include <stdbool.h>

int main() {
    int curr[5][5];
    int max_claim[5][5];
    int avl[5];
    int alloc[5] = { 0, 0, 0, 0, 0 };
    int max_res[5];
    int running[5];

    int i, j, exec, r, p;
    int count = 0;
    bool safe = false;

    printf ("\nEnter the number of resources: \n");
    scanf ("%d", &r);

    printf ("\nEnter the number of processes: \n");
    scanf ("%d", &p);
    for ( i = 0; i < p; i++ ) {
        running[i] = 1;
        count++;
    }

    printf("\nEnter Claim Vector: \n");
    for( i = 0; i < r; i++ ) {
        scanf ( "%d", &max_res[i] );
    }

    printf("\nEnter Allocated Resource Table: \n");
    for ( i = 0; i < p; i++) {
        for ( j = 0; j < r; j++ ) 
            scanf("%d", &curr[i][j]);
    }

    printf("\nEnter Maximum Claim table: \n");
    for ( i = 0; i < p; i++ ) {
        for( j = 0; j < r; j++ ) 
            scanf("%d", &max_claim[i][j]);
    }

    printf("\nThe Claim Vector is: \n");
    for( i = 0; i < r; i++ ) {
        printf( "%d", max_res[i] );
    }

    printf("\nThe Allocated Resource Table: \n");
    for ( i = 0; i < p; i++) {
        for ( j = 0; j < r; j++ ) 
            printf("%d", curr[i][j]);
        printf("\n");
    }
    
    printf("\nThe Maximum Claim table: \n");
    for ( i = 0; i < p; i++ ) {
        for( j = 0; j < r; j++ ) 
            printf("%d", max_claim[i][j]);
        printf("\n");
    }

    for ( i = 0; i < p; i++ )
        for ( j = 0; j < r; j++ ) 
            alloc[j] += curr[i][j];
    
    printf("\nAllocated resources: \n");
    for ( i = 0; i < r; i++ )
        printf("%d", alloc[i]);

    for ( i = 0; i < r; i++ )
        avl[i] = max_res[i] - alloc[i];

    printf("\nAvailable resources: \n");
    for ( i = 0; i < r; i++ )
        printf("%d", avl[i]);
    printf("\n");

    while( count != 0 ) {
        safe = false;
        for ( i = 0; i < p; i++ ) {
            if ( running[i] ) {
                exec = 1;
                for ( j = 0; j < r; j++ ) {
                    if ( max_claim[i][j] - curr[i][j] > avl[j] ) {
                        exec = 0;
                        break;
                    }
                }

                if ( exec ) {
                    printf("\n Process%d is executing.\n", i + 1 );
                    running[i] = 0;
                    count--;
                    safe = true;
                    for ( j = 0; j < r; j++ ) 
                        avl[j] += curr[i][j];
                    break;
                }
            }
        }

        if ( !safe ) {
            printf("\nThe processes are in unsafe state.");
            break;
        }

        if ( safe ) {
            printf("\nThe process is in safe state.\n");
        }

        printf("\nAvailable vector: ");

        for ( i = 0; i < r; i++ )
            printf("%d ", avl[i]);
    }

    return 0;
}
