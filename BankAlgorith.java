import java.util.ArrayList;
//import java.util.List;
import java.util.Scanner;

public class BankAlgorith {
    public static void main( String[] args ) {
        UserCli u = new UserCli();
        
        u.inputClaimResourceInfo();     

        //debug
        //u.outputClaimResourceInfo();

        u.inputProcessInfo();

        //debug
        u.outputProcessInfo();
        
        String res = BankAlgo.bankAlgorithm();
        if ( res != "" )
            Print.toScreenLn( "Running seq: " + res );
        else
            Print.toScreenLn( "Failed" );
    }
} 

class Resource {
    private int resIdx;
    private int insNum;

    Resource ( int rIdx, int iNum ) {
        resIdx     = rIdx;                         // resource id 
        insNum     = iNum;                         // how many instance of each resource
    }

    public int getResIndex() {
        return resIdx;
    }

    public void setResIndex( int idx ) {
        resIdx = idx;
    }

    public int getResInstance() {
        return insNum;
    }

    public void setResInstance( int ins ) {
        insNum = ins;
    }
}

class ResourceList {
    private static int numOfRes;
    private static ArrayList<Resource> clmResList = null;  //claim resource 
    private static ArrayList<Resource> avaResList = null;  //available resource

    ResourceList ( int resNum ) {
        numOfRes = resNum;

        if ( clmResList == null )
            clmResList = new ArrayList<Resource>( numOfRes );
        
        if ( avaResList == null ) {
            avaResList = new ArrayList<Resource>( numOfRes );

            // init avaResList will all 0
            for ( int idx = 0; idx < numOfRes; idx++ ) 
                avaResList.add( new Resource( idx, 0 ) );
        }
    }

    public void addToClaimResList( Resource r ) {
        if ( clmResList == null ) 
            throw new NullPointerException("init resource list first");
        
        clmResList.add(r);
    }

    public static ArrayList<Resource> getClaimResList() {
        return clmResList;
    }

    public static ArrayList<Resource> getAvaResList() {
        updateAvaResList();
        return avaResList;
    }

    private static void updateAvaResList() {
        int usedIns ;

        if ( avaResList == null )
            throw new NullPointerException("init available resource list first");
        
        if ( clmResList == null )
            throw new NullPointerException("init available resource list first");
            
        ArrayList<Process> pList = ProcessList.getProcessList();  
        if ( pList == null )
            throw new NullPointerException("init process list first");

        for ( int rId = 0; rId < avaResList.size(); rId++ ) {
            usedIns = 0;
            
            for ( int pIdx = 0; pIdx < pList.size(); pIdx++ ) {
                usedIns += ProcessList.getInstanceUsage( pIdx, rId );          
            }
            
            usedIns =  clmResList.get(rId).getResInstance() - usedIns;
            if ( usedIns < 0 ) {
                Print.toScreenLn( "Data Error" );
            }

            avaResList.set( rId,  new Resource( rId, usedIns ) ); 
        } 
    } 
}

class Process {
    private int proIdx;
    private boolean pState;
    private ArrayList<Resource> resList = null;
    private ArrayList<Resource> needResList = null;

    Process( int pIdx, boolean pS, int numOfRes ) {
        proIdx = pIdx;
        pState = pS;

        if ( resList == null ) 
            resList = new ArrayList<Resource>( numOfRes );
        
        if ( needResList == null )
            needResList = new ArrayList<Resource>( numOfRes );
    } 

    public void addToResList( Resource r ) {
        if ( resList == null )
            throw new NullPointerException("init resource list");
    
        resList.add(r);
    }

    public ArrayList<Resource> getResList() {
        return resList;
    }
    
    public void addToNeedResList( Resource r ) {
        if ( needResList == null ) 
            throw new NullPointerException("init resource list");
       
        needResList.add(r);
    }
    
    public ArrayList<Resource> getNeedResList() {
        return needResList;
    }

    public int getProcessId() {
        return proIdx;
    }

    public boolean getProcessState() {
        return pState;
    }

    public void setProcessState( boolean state ) {
        pState = state;
    }
}

class ProcessList {
    private int numOfProcess;
    private static ArrayList<Process> processList       = null;

    ProcessList() {}

    ProcessList( int processNum ) {
        numOfProcess = processNum;

        if ( processList == null ) {
            processList = new ArrayList<Process>( numOfProcess );
        }
    }
    
    public static void addToProcessList( Process p ) {
        if ( processList == null ) {
            throw new NullPointerException("init process list first");
        }

        processList.add(p);
    }

    public static ArrayList<Process> getProcessList(){
        return processList;
    }
    
    /**
     * get Instance number of a specified resource(rId) of a specified process(pId)
     */
    public static int getInstanceUsage( int pId, int rId ) { //return process pId usage of rId
        int ret = -1;
        boolean found = false;
        ArrayList<Resource> resList = null;
        int idx = 0;

        if( processList != null ) {
            for ( ; idx < processList.size(); idx++ ) {
                if ( pId == processList.get(idx).getProcessId() ) {
                    found = true;
                    break;
                }
            }

            if ( found && idx < processList.size() ) {  //found the specified process node
                resList = processList.get(idx).getResList();    // get resource list of specified node

                for ( int loop = 0; loop < resList.size(); loop++ ) {
                    if ( rId == resList.get(loop).getResIndex() ) {
                        ret = resList.get(loop).getResInstance();
                        break;
                    }
                } 
            }
        }        

        return ret;
    }

    /**
     * get Instance number of a specified resource(rId) of a specified process(pId)
     */
    public static boolean setInstanceUsage( int pId, int rId, int ins ) {
        boolean ret = false; 
        boolean found = false;
        ArrayList<Resource> resList = null;
        int idx = 0;

        if( processList != null ) {
            //locate a specified process node in processList (with pId)
            for (; idx < processList.size(); idx++ ) {
                if ( pId == processList.get(idx).getProcessId() ) {
                    found = true;
                    break;
                }
            }

            if ( found && idx < processList.size() ) {  //found the specified process node
                resList = processList.get(idx).getResList();    // get resource list of specified node

                for ( int loop = 0; loop < resList.size(); loop++ ) {
                    if ( rId == resList.get(loop).getResIndex() ) {
                        resList.get(loop).setResInstance( ins );
                        ret = true;
                        break;
                    }
                } 
            }
        }        

        return ret;
    }
}

class UserCli {
    private Scanner input; 
    private int numOfProcess;
    private int numOfResource;

    UserCli() {
        input = new Scanner(System.in); 
    }

    void inputClaimResourceInfo() {
        Print.toScreen( "---- Enter Number of Resource: " );
        numOfResource = input.nextInt();  

        // create resource list
        ResourceList rList = new ResourceList( numOfResource );

        for ( int i = 0; i < numOfResource; i++ ) {
            Print.toScreen("---- ---- Claim Instance for Resource " + (i+1) + ": " );

            Resource r = new Resource(i, input.nextInt() );
            rList.addToClaimResList( r );      
        }
    }
    
    void outputClaimResourceInfo() {
        Print.toScreenLn ("---- Claim Resource Information ----");

        ArrayList<Resource> rList = ResourceList.getClaimResList();

        if ( rList == null ) {
            throw new NullPointerException("init resource list first");
        }

        for ( int idx = 0; idx < rList.size(); idx++ ) {
            Print.toScreen  ( (idx+1) + " Node:");
            Print.toScreen  ( "Resource index: "    + rList.get(idx).getResIndex() + ", ");
            Print.toScreenLn( "Resource instance: " + rList.get(idx).getResInstance() );
        }
    }

    void inputProcessInfo() {
       Print.toScreenLn( "---- Enter Process Information ----" );
       Print.toScreen( "---- ---- Enter Total Number of Processes: " );
       numOfProcess = input.nextInt();        
       int instance;
       
       // create process list
       new ProcessList( numOfProcess );

       for ( int i = 0; i < numOfProcess; i++ ) {
           Process p = new Process( i /*id*/, true /*state*/, numOfResource /*num of ressource the process will use*/ ); // create a new process
           for ( int j = 0; j < numOfResource; j++ ) {
               Print.toScreen( "---- ---- ---- For Process " + (i+1) + ": Enter Number of Instance In-Use. ( Resource " + (j+1) + "): " ); 
               
               instance = input.nextInt();
               p.addToResList( new Resource( j, instance ) );     // add used resource to process list

               Print.toScreen( "---- ---- ---- For Process " + (i+1) + ": Enter Number of Instance Needed. ( Resource " + (j+1) + "): " ); 
               instance = input.nextInt();
               p.addToNeedResList( new Resource( j, instance ) );
           }

           ProcessList.addToProcessList( p ) ;
           Print.toScreenLn("");
       }
   } 
    
    void outputProcessInfo() {
        ArrayList<Process> pList = ProcessList.getProcessList();

        if ( pList == null ) {
            throw new NullPointerException("init resource list first");
        }

        for ( int i = 0; i < pList.size(); i++ ) {
            Print.toScreenLn( "Process: " + (i+1) );
            
            Process p = pList.get(i);               //get a specified process node
            
            ArrayList<Resource> rList = p.getResList();    // get associated resource list
            if ( rList == null )
                throw new NullPointerException("init resource list first");
            
            ArrayList<Resource> nList = p.getNeedResList();    // get associated resource list
            if ( nList == null )
                throw new NullPointerException("init needed resource list first");

            for ( int j = 0; j < rList.size(); j++ ) {
                Print.toScreenLn("  Used Resource   " + (j+1) + ": " + rList.get(j).getResInstance() ); //traverse instance of associated process 
                Print.toScreenLn("  Needed Resource " + (j+1) + ": " + nList.get(j).getResInstance() ); //traverse instance of associated process 
            }

            Print.toScreenLn("");
        }
    }
}

class BankAlgo {
    public static String bankAlgorithm() {
        String runningList = "";
        boolean match ;
        ArrayList<Resource> aList ; 
        ArrayList<Resource> nList ;

        ArrayList<Process> pList = ProcessList.getProcessList();
        if ( pList == null ) 
            throw new NullPointerException("init process resource list first");

        int count = pList.size();
        while( count > 0 ) {
        for ( int pId = 0; pId < pList.size(); pId++ ) {
            aList = ResourceList.getAvaResList();
            match = true;
            
            if ( aList == null ) 
                throw new NullPointerException("init available resource list first");

            if ( pList.get(pId).getProcessState() == true ) {
                nList = pList.get(pId).getNeedResList();  
                
                if ( nList == null )
                    throw new NullPointerException("init need resource list first");
                
                for ( int rId = 0; rId < nList.size() ; rId++ ) {
                    if ( nList.get(rId).getResInstance() > aList.get(rId).getResInstance() ) {  //cannot satisfy
                        Print.toScreen("!!" + pId);
                        match = false;
                        break;
                    }
                }
                 
                if ( match ) {
                   runningList += "Process"+ pId + "," ;
                   pList.get(pId).setProcessState(false);   //set process state to false
                   
                   //release resouce 
                   for ( int rId = 0; rId< nList.size(); rId++ )
                       ProcessList.setInstanceUsage( pId, rId, 0 ) ;
                }
            }
        }
        count--;
        } 

        return runningList;
    }    
}

class Print {
    public static void toScreenLn( String s ) {
        System.out.println( s );
    }
    
    public static void toScreen( String s ) {
        System.out.print( s );
    }
}
